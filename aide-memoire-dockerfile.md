# Liste des commandes utilisables dans un dockerfile
- FROM : Image parente
- MAINTAINER : Auteur
- ARG : Variables passées comme paramètres à la construction de l'image
- ENV : Variable d'environnement
- LABEL : Ajout de métadonnées
- VOLUME : Crée un point de montage
- RUN : Commande(s) utilisée(s) pour construire l'image
- ADD : Ajoute un fichier dans l'image
- COPY : Ajoute un fichier dans l'image
- WORKDIR : Permet de changer le chemin courant
- EXPOSE 	Port(s) écouté(s) par le conteneur
- USER : Nom d'utilisateur ou UID à utiliser
- ONBUILD : Instructions exécutées lors de la construction d'images enfants
- CMD : Exécuter une commande au démarrage du conteneur
- ENTRYPOINT : Exécuter un script au démarrage du conteneur


# Ressources 
- The Difference between COPY and ADD in a Dockerfile : https://nickjanetakis.com/blog/docker-tip-2-the-difference-between-copy-and-add-in-a-dockerile