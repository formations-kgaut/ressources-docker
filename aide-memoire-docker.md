# Docker - Images

## Lancer une image
```bash
docker run IMAGE
```

## Construire une image
Dans un dossier contenant un Dockerfile :
```bash
docker build .
```

## Construction d'une image en spécifiant le Dockerfile
Avec un fichier `Dockerfile-custom` :
```bash
Docker build -f Dockerfile-custom .
```

## Construire une image en la taguant
Dans un dossier contenant un Dockerfile :

```bash
docker build -t mon_tag .
```

## Taguer une image existante
```bash
docker tag TAG_EXISTANT NOUVEAU_TAG
```

## Point de montage
Option `-v SOURCE_HOST:CIBLE_CONTAINER`

`${PWD}` : chemin absolu vers le dossier courant

```bash
docker run -v ${PWD}/dossier:/app test-montage
```

## Le transfert de port / Port Forwarding

Option `-v PORT_HOST:PORT_CIBLE_CONTAINER`

ex :
```bash
docker run -p 8081:80 -v ${PWD}:/app php:8.1-cli php -S 0.0.0.0:80 /app/script.php
```

http://localhost:8081/

## Connection dans un conteneur
```bash
docker run -it IMAGE COMMANDE
```
ex :
```bash
docker run -it mon_hello_world /bin/sh
```

# Docker compose

## Lancer les containers
```bash
docker compose up
```

## Lancer les containers en mode détaché
```bash
docker compose up -d
```

## stopper les containers
```bash
docker compose down
```

## Exectuer des commandes dans un container
```
docker compose exec CONTAINER COMMAND
```

ex :
```
docker compose exec mariadb3 /bin/bash
```

## Affichage des logs
```bash
docker compose logs
```
Suivi des erreurs :
```bash
docker compose logs -f
```
Logs d'un seul container
```bash
docker compose logs web -f
```
