# Lancer les containers
```bash
docker compose up
```

# Arrêter les containers
```bash
docker compose down
```