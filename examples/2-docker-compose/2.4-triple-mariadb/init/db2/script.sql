CREATE DATABASE db2;

GRANT ALL PRIVILEGES ON db2.* TO 'utilisateur'@'%';

USE db2;

CREATE TABLE `data` (
  `id` int(11) NOT NULL,
  `label` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `data` ADD PRIMARY KEY (`id`);

ALTER TABLE `data` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
