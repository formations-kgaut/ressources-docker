image source : https://hub.docker.com/_/php

# Utiliser le serveur PHP 

Transfert du port 8081 => 80 dans le container

```bash
docker run -p 8081:80 -v ${PWD}:/app php:8.1-cli php -S 0.0.0.0:80 /app/script.php
```

## Avec une autre version de php
```bash
docker run -p 8074:80 -v ${PWD}:/app php:7.4-cli php -S 0.0.0.0:80 /app/script.php
```