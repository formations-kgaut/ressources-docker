# Construction
```bash
docker build -f Dockerfile-v1 . -t mon_hello_world:1.0.0
docker build -f Dockerfile-v2 . -t mon_hello_world:2.0.0
```

# Lancement
```bash
docker run mon_hello_world:2.0.0
docker run mon_hello_world:1.0.0
```