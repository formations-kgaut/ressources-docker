# Construction
```bash
docker build . -t test-montage
```

# Lancement sans point de montage
```bash
docker run test-montage
```

# Lancement sans point de montage
```bash
docker run test-montage
```

# Lancement avec point de montage
```bash
docker run -v ${PWD}/dossier:/app test-montage
```