# Construction
```bash
docker build . -t kgaut/test
```

# Authentification sur hub.docker
```
docker login -u kgaut
```
Possibilité d'utiliser un access token : https://hub.docker.com/settings/security

# Push de l'image
```bash
docker push kgaut/test
```
