# Connexion dans le container
```bash
docker run -it -v ${PWD}:/app node:14 /bin/bash
```

# Installation des dépendances
```bash
cd /app
npm install
```

# Compilation
```bash
npm run sass-compile
```